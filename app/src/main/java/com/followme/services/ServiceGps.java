package com.followme.services;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.followme.model.beans.GeoPunto;
import com.followme.util.GlobalConstants;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by root on 26/05/17.
 */

public class ServiceGps implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Context context;
    private Activity activity;
    //public static Location currentLocation;
    //public static GeoPunto currentGeoPunto=new GeoPunto();
    public static Boolean isActiveGps = false;
    public static final String TAG = ServiceGps.class.getSimpleName();
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private int minutes;
    public static boolean aceptoUsoGeolocalizacion = false;


    public ServiceGps(Activity activity, Context context, Integer minutes) {
        this.activity = activity;
        this.context = context;
        this.minutes = 1000 * 60 * minutes;
        init(context);
    }

    private void init(Context context) {
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds
    }

    /**
     * execute in onResume of the Activity
     */
    public void connect() {
        mGoogleApiClient.connect();
    }

    public boolean isConected() {
        return mGoogleApiClient.isConnected();
    }

    /**
     * execute in onPause of the Activity
     */
    public void disconnect() {
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, (LocationListener) this);
            mGoogleApiClient.disconnect();
        }
    }

    private void handleNewLocation(Location location) {
        if (isBetterLocation(location,  GlobalConstants.currentLocationFused )) {
            Log.d(TAG, location.toString());
            //currentLocation = location;
            GlobalConstants.currentTimeLocationFused=(new java.util.Date()).getTime();
            GlobalConstants.currentLocationFused = location;
            //currentGeoPunto.setLatitude(currentLocation.getLatitude());
            //currentGeoPunto.setLongitude(currentLocation.getLongitude());
            //currentGeoPunto.setLocation(currentLocation);
        }
    }

    /*inicio de metodos de geolocalizacion*/
    @Override
    public void onLocationChanged(Location location) {
        handleNewLocation(location);
    }
    /*Fin de metodos de geolocalizacion*/

    /*inicio de metodos de google apis*/
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        try {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (location == null) {
                isActiveGps = false;
                Log.d(TAG + "isactivegps", isActiveGps.toString());
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            } else {
                isActiveGps = true;
                Log.d(TAG + "isactivegps", isActiveGps.toString());
                handleNewLocation(location);
            }
        } catch (Exception ex) {
            isActiveGps = false;
            Log.d(TAG + "ONCONNECTED", ex.getMessage());
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG + "onConnectionSuspended", String.valueOf(i));
    }

    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > minutes;
        boolean isSignificantlyOlder = timeDelta < -minutes;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /**
     * Checks whether two providers are the same
     */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(activity, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }


    public boolean activarGeolocalizacion(final Activity actividad, final Integer codeUi) {
        if (mGoogleApiClient != null) {
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .setAlwaysShow(true)
                    .addLocationRequest(mLocationRequest);

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            if (ActivityCompat.checkSelfPermission(actividad.getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(actividad.getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                return;
                            }
                            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, ServiceGps.this);
                            aceptoUsoGeolocalizacion = true;
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                status.startResolutionForResult(
                                        actividad, codeUi);
                            } catch (IntentSender.SendIntentException e) {
                                aceptoUsoGeolocalizacion = false;
                            }

                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            aceptoUsoGeolocalizacion = false;
                            break;
                    }
                }
            });
        }
        return aceptoUsoGeolocalizacion;
    }
}
