package com.followme.services;

/**
 * Created by root on 26/05/17.
 */

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.followme.model.beans.GeoPunto;
import com.followme.util.GlobalConstants;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by root on 06/09/16.
 */
public class ServiceGeolocalizacion implements LocationListener {
    private LocationManager locationManager;
    private Context context;
    //public static Location currentLocation;
    //public static GeoPunto currentGeoPunto=new GeoPunto();
    public static Boolean isActiveService = false;
    private Criteria criterio;
    private String locationProvider;
    public static final String TAG = ServiceGeolocalizacion.class.getSimpleName();
    private int minutes;


    public ServiceGeolocalizacion(Context context, Integer minutes) {
        this.context = context;
        this.minutes = 1000 * 60 * minutes;
        init(context);
    }

    private void init(Context context) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    }

    public boolean onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        criterio = new Criteria();
        criterio.setSpeedRequired(true);//false
        criterio.setSpeedAccuracy(Criteria.ACCURACY_HIGH);//Criteria.NO_REQUIREMENT
        criterio.setVerticalAccuracy(Criteria.ACCURACY_HIGH);//Criteria.NO_REQUIREMENT
        criterio.setAccuracy(Criteria.ACCURACY_FINE);
        criterio.setBearingRequired(true);//false
        criterio.setBearingAccuracy(Criteria.ACCURACY_HIGH);//Criteria.NO_REQUIREMENT
        criterio.setHorizontalAccuracy(Criteria.ACCURACY_HIGH);
        criterio.setPowerRequirement(Criteria.POWER_HIGH);
        criterio.setCostAllowed(false);
        criterio.setAltitudeRequired(true);
        try {
            locationProvider = locationManager.getBestProvider(criterio, true);
        } catch (Exception ex) {
            isActiveService = false;
            return isActiveService;
        }
        locationManager.requestLocationUpdates(locationProvider, 0, 0, this);
        Location lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);
        if (lastKnownLocation == null) {
            isActiveService = false;
            Log.d(TAG + "isactivegps", isActiveService.toString());
            locationManager.removeUpdates(this);
        } else {
            isActiveService = true;
            Log.d(TAG + "isactivegps", isActiveService.toString());
            handleNewLocation(lastKnownLocation);
        }
        return isActiveService;
    }

    public boolean isConected() {
        return locationManager.isProviderEnabled(locationProvider);
    }

    public void disconnect() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }
    }

    public void handleNewLocation(Location location) {
        if (isBetterLocation(location, GlobalConstants.currentLocationGps)) {
            Log.d(TAG, location.toString());
            //currentLocation = location;
            GlobalConstants.currentTimeLocationGps=(new java.util.Date()).getTime();
            GlobalConstants.currentLocationGps=location;
            //currentGeoPunto.setLatitude(currentLocation.getLatitude());
            //currentGeoPunto.setLongitude(currentLocation.getLongitude());
            //currentGeoPunto.setLocation(currentLocation);
        }

    }

    @Override
    public void onLocationChanged(Location location) {
        handleNewLocation(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d(TAG + "Provider", provider);
        Log.d(TAG + "status", String.valueOf(status));
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d(TAG + "onProviderEnabled", provider);
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d(TAG + "onProviderDisabled", provider);
    }

    public Boolean getActiveService() {
        return isActiveService;
    }

    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > minutes;
        boolean isSignificantlyOlder = timeDelta < -minutes;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /**
     * Checks whether two providers are the same
     */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }


}

