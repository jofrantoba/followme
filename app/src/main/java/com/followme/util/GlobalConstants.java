package com.followme.util;

import android.app.Application;
import android.location.Location;

/**
 * Created by root on 02/06/17.
 */

public class GlobalConstants extends Application {
    public static Location currentLocationGps;
    public static Long currentTimeLocationGps;
    public static Location currentLocationFused;
    public static Long currentTimeLocationFused;
    public static Location currentLocationMap;
    public static Long currentTimeLocationMap;
}
